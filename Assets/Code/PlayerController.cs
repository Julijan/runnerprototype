﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    
    
    private PlayerConfig PlayerConfig;
    [SerializeField] private PlayerSimulationController PlayerSimulationController;

    public void StartSimulation() {
        PlayerSimulationController.StartSimulation(PlayerConfig);
    }

    public void UpdateSimulation() {
        PlayerSimulationController.UpdateSimulation();
    }

    public void OnEnter(Vector3 startPos, Action finishFunction) {
        PlayerConfig = new PlayerConfig();
        PlayerSimulationController.OnEnter(startPos, finishFunction);
    }

    public void ResetPlayer() {
        PlayerSimulationController.ResetPlayer();
    }
    
}
