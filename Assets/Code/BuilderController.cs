﻿using System.Collections.Generic;
using UnityEngine;

public class BuilderController : MonoBehaviour {

    [SerializeField] private PlacableTypeToGameObjectDictionary PlacablePrefabs;

    private PlacableType PlacableTypeSelected = PlacableType.None;
    private LayerMask BuilderLayerMask;
    private List<GameObject> PlacedPlacables = new List<GameObject>();
    
    public Vector3 GetHitPoint() {
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo, float.MaxValue, BuilderLayerMask)) {
                return hitInfo.point;
            }
        }
        return Vector3.negativeInfinity;
    }

    public PlacableType SpawnSelectedPlaceable(Vector3 position) {
        GameObject placable = Instantiate(PlacablePrefabs[PlacableTypeSelected], position, PlacablePrefabs[PlacableTypeSelected].transform.rotation);
        PlacedPlacables.Add(placable);
        return PlacableTypeSelected;
    }
    
    public void SpawnPlaceable(Vector3 position, PlacableType placableType) {
        Instantiate(PlacablePrefabs[placableType], position, PlacablePrefabs[placableType].transform.rotation);
    }

    public void SetPlacableTypeSelected(PlacableType placableType) {
        PlacableTypeSelected = placableType;
    }

    public bool IsAnyPlacableSelected() {
        return PlacableTypeSelected != PlacableType.None;
    }

    public PlacableType GetPlacableType() {
        return PlacableTypeSelected;
    }

    public void OnEnter(List<Vector3> finishNodePositions) {
        foreach (var finishNodePosition in finishNodePositions) {
            SpawnPlaceable(finishNodePosition, PlacableType.FinishLine);
        }
        BuilderLayerMask = 1 << LayerMask.NameToLayer("Lane");
    }

    public void ResetBuilder() {
        foreach (var placedPlacable in PlacedPlacables) {
            Destroy(placedPlacable);
        }
        PlacedPlacables.Clear();
    }
        
}
