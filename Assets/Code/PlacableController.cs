﻿using System.Collections.Generic;
using UnityEngine;

public class PlacableController : MonoBehaviour {
    
    private List<PlacableType> PlacableNodes = new List<PlacableType>();
    public void CreatePlacableGrid(int nodeCount) {
        for (int i = 0; i < nodeCount + 1; i++) {
            PlacableNodes.Add(PlacableType.None);
        }
    }

    public bool IsPlacableFree(int index) {
        return PlacableNodes[index] == PlacableType.None;
    }

    public void SetPlacable(int index, PlacableType placableType) {
        PlacableNodes[index] = placableType;
    }

    public void ResetPlacableData() {
        for (int i = 0; i < PlacableNodes.Count; i++) {
            PlacableNodes[i] = PlacableType.None;
        }
    }
}
