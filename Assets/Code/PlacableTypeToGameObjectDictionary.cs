﻿using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[Serializable]
public class PlacableTypeToGameObjectDictionary : SerializableDictionaryBase<PlacableType, GameObject> { }
