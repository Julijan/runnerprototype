﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerSimulationController : MonoBehaviour {
    
    [SerializeField] private Transform Transform;
    [SerializeField] private Rigidbody Rigidbody;

    public float Speed;
    public Vector3 MoveDirection;
   
    private LayerMask FinishLineLayerMask;
    private LayerMask BoosterLayerMask;
    private LayerMask ObsticleLayerMask;
    private bool SimulationRunning;
    private bool BlockRunning;
    private bool HasBoost;
    private Vector3 StartPos;
    private Action FinishFunction;
    private PlayerConfig PlayerConfig;
    
    public void StartSimulation(PlayerConfig playerConfig) {
        SimulationRunning = true;
        PlayerConfig = playerConfig;
    }
    
    public void StopSimulation() {
        SimulationRunning = false;
    }

    private void FinishRun() {
        StopSimulation();
        FinishFunction();
    }

    public void ResetPlayer() {
        StopSimulation();
        ResetPlayerToStartPos();
    }

    public void UpdateSimulation() {
        if (SimulationRunning) {
            if (!BlockRunning) {
                MovePlayer();
            }
        }
    }

    public void MovePlayer() {
        float tempSpeed = HasBoost ? Speed * 1.5f : Speed;
        Rigidbody.MovePosition(Transform.position + Time.deltaTime * tempSpeed * MoveDirection);
    }
    
    public void OnEnter(Vector3 startPos, Action finishFunction) {
        StartPos = startPos;
        ResetPlayerToStartPos();
        FinishFunction = finishFunction;
        FinishLineLayerMask = LayerMask.NameToLayer("FinishLine");
        ObsticleLayerMask = LayerMask.NameToLayer("Obsticle");
        BoosterLayerMask = LayerMask.NameToLayer("Booster");
    }

    public void ResetPlayerToStartPos() {
        Transform.position = new Vector3(StartPos.x, StartPos.y + 0.5f, StartPos.z);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == FinishLineLayerMask) {
            FinishRun();
        } else if (other.gameObject.layer == ObsticleLayerMask) {
            TryToJump();
        } else if (other.gameObject.layer == BoosterLayerMask) {
            HasBoost = true;
            StartCoroutine(StopBoostInNSeconds(1));
        }
    }
    
    IEnumerator BlockRunningForNSeconds(int seconds) {
        yield return new WaitForSeconds(seconds);
        TryToJump();
    }
    
    IEnumerator StopBoostInNSeconds(int seconds) {
        yield return new WaitForSeconds(seconds);
        HasBoost = false;
    }

    private void TryToJump() {
        float percentOfFail = PlayerConfig.ObsticleFailChance;

        float diceRoll = Random.Range(0f, 1f);

        if (diceRoll < percentOfFail) {
            if (Transform.gameObject.GetComponent<MeshRenderer>().material.color == Color.blue) {
                Transform.gameObject.GetComponent<MeshRenderer>().material.color = Color.red;
            } else {
                Transform.gameObject.GetComponent<MeshRenderer>().material.color = Color.blue;
            }
            BlockRunning = true;
            StartCoroutine(BlockRunningForNSeconds(1));
        } else {
            BlockRunning = false;
        }
    }
    
}
