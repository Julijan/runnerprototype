﻿using UnityEngine;
using UnityEngine.UI;

public class RunnerUIController : MonoBehaviour {

    [SerializeField] private GameObject BuilderPanel;
    [SerializeField] private GameObject BuildButton;
    [SerializeField] private GameObject PreperationUIElements;
    [SerializeField] private Toggle ObsticleToggle;
    [SerializeField] private Toggle BoosterToggle;
    
    private PlacableTypeTypeToToggleDictionary Toggles;

    public void ToggleBuilderEnabled(bool enable) {
        BuilderPanel.SetActive(enable);
        BuildButton.SetActive(!enable);
    }

    public void TogglePreperationUIElements(bool enable) {
        PreperationUIElements.SetActive(enable);
    }

    public void ResetGame() {
        PreperationUIElements.SetActive(true);
        ToggleBuilderEnabled(false);
        ObsticleToggle.isOn = false;
        BoosterToggle.isOn = false;
    }

    public void OnEnter(PlacableTypeTypeToToggleDictionary toggles) {
        Toggles = toggles;
        BuilderPanel.SetActive(false);
    }

    public void UnsetTogglesThatAreNotThisOne(PlacableType placableType) {
        foreach (var toggle in Toggles) {
            if (toggle.Key != placableType) {
                toggle.Value.isOn = false;
            }
        }
    }
}
