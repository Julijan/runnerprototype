﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class GridController : MonoBehaviour {
    
    public Transform StartNodeTransform;
    public Transform EndNodeTransform;
    
    public List<Vector3> Nodes = new List<Vector3>();
    
    [SerializeField] private NodeForSpaceController NodeForSpace;
    private List<NodeForSpaceController> CashedNodesForSpace = new List<NodeForSpaceController>();

    private Dictionary<PlacableType, RuleForPlacing> RulePerType = new Dictionary<PlacableType, RuleForPlacing>() {
        {PlacableType.Obsticle, RuleForPlacing.ObsticleRule},
        {PlacableType.Booster, RuleForPlacing.BoosterPlacingRule},
    };
    
    public void CreateGrid(int nodeCount) {
        Vector3 startNode = StartNodeTransform.position;
        Vector3 endNode = EndNodeTransform.position;
        
        float distance = (endNode.x - startNode.x);
        float margin = distance / nodeCount;
        for (int i = 1; i < nodeCount; i++) {
            float newPosition = startNode.x + margin * i;
            Vector3 node = new Vector3(newPosition, startNode.y, startNode.z);
            Nodes.Add(node);
        }
    }

    public void HighlightPlacableNodesForType(PlacableType placableType, PlacableController placableController) {
        HighlightPlacableNodes(placableType, placableController);
        RemoveUsedPlacableNodes(placableController);
    }

    public void RemoveUsedPlacableNodes(PlacableController placableController) {
        for (int i = 0; i < CashedNodesForSpace.Count; i++) {
            bool placeFree = placableController.IsPlacableFree(i);
            if (!placeFree) {
                RemoveNode(i);
            }
        }
    }

    public void RemoveNode(int index) {
        CashedNodesForSpace[index].ToggleEnabled(false);
    }

    public void HighlightPlacableNodes(PlacableType placableType, PlacableController placableController) {
        for (int i = 0; i < CashedNodesForSpace.Count; i ++) {
            bool isThisNodeForThisPlacable = IsThisNodeForThisPlacableType(i, placableType);
            CashedNodesForSpace[i].ToggleActive(isThisNodeForThisPlacable);
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        foreach (var node in Nodes) {
            Gizmos.DrawSphere(node, 0.2f);
        }
    }

    public void ToggleNodesForBuildingEnabled(bool enabled) {
        for (int i = 0; i < CashedNodesForSpace.Count; i++) {
            CashedNodesForSpace[i].ToggleEnabled(enabled);
        }
    }
    
    public void ToggleNodesForBuildingActive(bool enabled) {
        for (int i = 0; i < CashedNodesForSpace.Count; i++) {
            CashedNodesForSpace[i].ToggleActive(enabled);
        }
    }

    public void CreateNodesForBuilding() {
        for (int i = 0; i < Nodes.Count; i++) {
            if (i < CashedNodesForSpace.Count) {
                CashedNodesForSpace[i].transform.position = Nodes[i];
            } else {
                CashedNodesForSpace.Add(Instantiate(NodeForSpace, Nodes[i], Quaternion.identity));
            }
        }

        ToggleNodesForBuildingEnabled(false);
    }

    public bool IsThisNodeForThisPlacableType(int index, PlacableType placableType) {
        RuleForPlacing rule = RulePerType[placableType];
        if(index % 2 == 0) {
            return rule == RuleForPlacing.BoosterPlacingRule;
        } 
        return rule == RuleForPlacing.ObsticleRule;
    }

    public Vector3 GetStartPosOfLane() {
        return StartNodeTransform.position;
    }
    
    public Vector3 GetEndPosOfLane() {
        return EndNodeTransform.position;
    }
}
