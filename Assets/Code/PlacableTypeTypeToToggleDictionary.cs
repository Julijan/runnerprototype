﻿using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine.UI;

[Serializable]
public class PlacableTypeTypeToToggleDictionary : SerializableDictionaryBase<PlacableType, Toggle> { }
