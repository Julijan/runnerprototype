﻿using UnityEngine;

public class NodeForSpaceController : MonoBehaviour {

    [SerializeField] private MeshRenderer MeshRenderer;
    
    public void ToggleActive(bool enable) {
        if (enable) {
            gameObject.transform.localScale = new Vector3(0.8f,0.8f,0.8f);
            MeshRenderer.material.color = Color.yellow;
        }
        else {
            gameObject.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
            MeshRenderer.material.color = Color.gray;
        }
    }

    public void ToggleEnabled(bool enable) {
        gameObject.SetActive(enable);
        ToggleActive(false);
    }
    
}
