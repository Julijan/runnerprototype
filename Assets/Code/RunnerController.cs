﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RunnerController : MonoBehaviour {

    [SerializeField] private List<LaneController> LaneControllers;
    [SerializeField] private BuilderController BuilderController;
    [SerializeField] private List<PlayerController> Players;

    public void OnEnter(Action finishFunction) {
        List<Vector3> finishPositions = new List<Vector3>();
        
        foreach (var laneController in LaneControllers) {
            laneController.OnEnter();
            finishPositions.Add(laneController.GetEndPosOfLane());
        }
        
        BuilderController.OnEnter(finishPositions);
        
        for (int i = 0; i < Players.Count; i++) {
            Players[i].OnEnter(LaneControllers[i].GetStartPosOfLane(), finishFunction);
        }
        
        CreateNodesForBuilding();
    }
    
    public void OnUpdate() {
        foreach (var player in Players) {
            player.UpdateSimulation();
        }
    }
    
    private void CreateNodesForBuilding() {
        foreach (var laneController in LaneControllers) {
            laneController.CreateNodesForBuilding();
        }
    }
    
    public void OnLeftClick() {
        if (!BuilderController.IsAnyPlacableSelected()) return;
        Vector3 hitPoint = BuilderController.GetHitPoint();
        if (!hitPoint.Equals(Vector3.negativeInfinity)) {
            Tuple<int,int> closestIndex = GetIndexesOfTheClosestNode(hitPoint);
            bool placeFree = LaneControllers[closestIndex.Item1].IsPlacablePlaceFree(closestIndex.Item2);
            bool isCorrectNode = LaneControllers[closestIndex.Item1]
                .IsThisNodeForThisPlacableType(closestIndex.Item2, BuilderController.GetPlacableType());
            if (placeFree && isCorrectNode) {
                Vector3 pos = LaneControllers[closestIndex.Item1].GetNode(closestIndex.Item2);
                PlacableType placablePlaced = BuilderController.SpawnSelectedPlaceable(pos);
                LaneControllers[closestIndex.Item1].SetPlacable(closestIndex.Item2,placablePlaced);
                LaneControllers[closestIndex.Item1].RemoveNode(closestIndex.Item2);
            }
        }
    }
    
    private Tuple<int,int> GetIndexesOfTheClosestNode(Vector3 hitPoint) {
        float smallestDistance = float.MaxValue;
        int iIndex = 0;
        int jIndex = 0;
        for (int i = 0; i < LaneControllers.Count; i++) {
            List<Vector3> nodes = LaneControllers[i].GetGridOfThisLane();
            for (int j = 0; j < nodes.Count; j++) {
                float tempDistance = Vector3.Distance(nodes[j], hitPoint);
                if (smallestDistance > tempDistance) {
                    smallestDistance = tempDistance;
                    iIndex = i;
                    jIndex = j;
                }
            }
        }
        return new Tuple<int, int>(iIndex,jIndex);
    }

    public void OnStartClick() {
        foreach (var laneController in LaneControllers) {
            laneController.ToggleNodesForBuildingEnabled(false);
        }
        foreach (var player in Players) {
            player.StartSimulation();
        }
    }

    public void OnPlacableSelected(PlacableType placableType) {
        BuilderController.SetPlacableTypeSelected(placableType);
        foreach (var laneController in LaneControllers) {
            laneController.HighlightPlacableNodesForType(placableType);
        }
    }

    public void UnsetPlacableSelected() {
        BuilderController.SetPlacableTypeSelected(PlacableType.None);
    }

    public void ResetGame() {
        foreach (var laneController in LaneControllers) {
            laneController.ResetLane();
        }

        foreach (var player in Players) {
            player.ResetPlayer();
        }
        
        BuilderController.ResetBuilder();
        UnsetPlacableSelected();
    }

    public void ToggleBuilderEnabled(bool enable) {
         foreach (var laneController in LaneControllers) {
             laneController.ToggleNodesForBuildingEnabled(enable);
         }
    }
    
    public void ToggleNodesForBuildingActive(bool enable) {
         foreach (var laneController in LaneControllers) {
             laneController.ToggleNodesForBuildingActive(enable);
         }
    }
}
