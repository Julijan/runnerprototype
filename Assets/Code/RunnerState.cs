﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class RunnerState : MonoBehaviour {

    [SerializeField] private Button StartButton;
    [SerializeField] private Button BuildButton;
    [SerializeField] private RunnerController RunnerController;
    [SerializeField] private RunnerUIController RunnerUIController;
    [SerializeField] private PlacableTypeTypeToToggleDictionary Toggles;
    
    private void Start() {
        OnEnter();
    }

    void OnEnter() {
        StartButton.onClick.AddListener(OnStartClick);
        foreach (var toggle in Toggles) {
            toggle.Value.onValueChanged.AddListener(delegate { OnObsticleSelected(toggle.Key
            ); });
        }
        BuildButton.onClick.AddListener(EnableBuilding);
        RunnerController.OnEnter(FinishRun);
        RunnerUIController.OnEnter(Toggles);
    }

    private void Update() {
        RunnerController.OnUpdate();
        bool leftClick = Input.GetMouseButtonDown(0);
        if (leftClick) {
            RunnerController.OnLeftClick();
        }
    }

    void OnStartClick() {
        RunnerController.OnStartClick();
        RunnerUIController.TogglePreperationUIElements(false);
    }

    void FinishRun() {
        ResetGame();
    }

    void ResetGame() {
        RunnerController.ResetGame();
        RunnerUIController.ResetGame();
    }
    
    void OnObsticleSelected(PlacableType placableType) {
        if (Toggles[placableType].isOn) {
            RunnerController.OnPlacableSelected(placableType);
            RunnerUIController.UnsetTogglesThatAreNotThisOne(placableType);
        } 
    }

    void EnableBuilding() {
        RunnerController.ToggleBuilderEnabled(true);
        RunnerUIController.ToggleBuilderEnabled(true);
    }
}
