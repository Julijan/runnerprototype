﻿using System.Collections.Generic;
using UnityEngine;

public class LaneController : MonoBehaviour {

    [SerializeField] private GridController GridController;
    [SerializeField] private PlacableController PlacableController;

    public int NodeCount = 12;
    
    public void OnEnter() {
        GridController.CreateGrid(NodeCount);
        PlacableController.CreatePlacableGrid(NodeCount);
    }

    public List<Vector3> GetGridOfThisLane() {
        return GridController.Nodes;
    }
    
    public Vector3 GetNode(int index) {
        return GridController.Nodes[index];
    }

    public bool IsPlacablePlaceFree(int index) {
        return PlacableController.IsPlacableFree(index);
    }

    public void SetPlacable(int index, PlacableType placableType) {
        PlacableController.SetPlacable(index, placableType);
    }

    public void HighlightPlacableNodesForType(PlacableType placableType) {
        GridController.HighlightPlacableNodesForType(placableType, PlacableController);
    }
    
    public void ToggleNodesForBuildingEnabled(bool enable) {
        GridController.ToggleNodesForBuildingEnabled(enable);
    }
    
    public void ToggleNodesForBuildingActive(bool enable) {
        GridController.ToggleNodesForBuildingActive(enable);
    }

    public void RemoveNode(int index) {
        GridController.RemoveNode(index);
    }

    public bool IsThisNodeForThisPlacableType(int index, PlacableType placableType) {
        return GridController.IsThisNodeForThisPlacableType(index, placableType);
    }

    public Vector3 GetStartPosOfLane() {
        return GridController.GetStartPosOfLane();
    }
    
    public Vector3 GetEndPosOfLane() {
        return GridController.GetEndPosOfLane();
    }

    public void CreateNodesForBuilding() {
        GridController.CreateNodesForBuilding();
    }

    public void ResetLane() {
        PlacableController.ResetPlacableData();
    }
}
